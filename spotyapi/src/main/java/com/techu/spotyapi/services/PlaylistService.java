package com.techu.spotyapi.services;

import com.techu.spotyapi.models.PlaylistModel;
import com.techu.spotyapi.repositories.PlaylistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PlaylistService {
    
    @Autowired
    PlaylistRepository playlistRepository;
    @Autowired
    SongService songService;
    @Autowired
    UserAccountService userAccountService;

    public List<PlaylistModel> findAll(String userAccount) {
        List<PlaylistModel> result;
        System.out.println("findAll en PlaylistService");
        System.out.println("en playlistservice el id de usuario vale "+userAccount);

        if (userAccount == null) {
            result = this.playlistRepository.findAll();
        }
        else {
            result = findAllByUserAccount(userAccount);
            return result;
        }
        return result;
    }

    private List<PlaylistModel> findAllByUserAccount(String userAccount) {
        System.out.println("findAllByUserAccount en PlayListService");
        System.out.println("en findAllByUserAccount el id de usuario es "+userAccount);
        List<PlaylistModel> allPlaylist = this.playlistRepository.findAll();
        List<PlaylistModel> userPlaylist = new ArrayList<PlaylistModel>() ;
        for (PlaylistModel i : allPlaylist) {
            System.out.println("recorriendo listas");
            System.out.println("la cuenta de usuario de la lista es "+i.getUserId()+ "el solicitado es "+userAccount);
            if (i.getUserId().equals(userAccount) ) {
                userPlaylist.add(i);
            }
        }
        return userPlaylist;

    }

    public Optional<PlaylistModel> findById(String id) {
        System.out.println("findById en PlaylistService");
        return this.playlistRepository.findById(id);
    }

    public PlaylistServiceResponse addPlaylist(PlaylistModel playlistModel){
        System.out.println("addPlaylist en PlaylistService");

        PlaylistServiceResponse result = new PlaylistServiceResponse();

        result.setPlaylist(playlistModel);

        if (this.userAccountService.findById(playlistModel.getUserId()).isPresent()!= true) {
            System.out.println("El usuario de la playlist no se ha encontrado");

            result.setMsg("El usuario de la playlist no se ha encontrado");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

            return result;
        }

        if (this.playlistRepository.findById(playlistModel.getId()).isPresent() == true) {
            System.out.println("Ya hay una playlist con esa id");

            result.setMsg("Ya hay una playlist con esa id");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

            return result;
        }

        int totalDuration = 0;
        ArrayList<String> lista = playlistModel.getSongList();

        for (int i=0;i<lista.size();i++) {
            if(!this.songService.findById(lista.get(i)).isPresent()) {
                System.out.println("La canción con el id " + lista.get(i)
                        + " no se encuentra en el sistema");

                result.setMsg("La canción con la id " + lista.get(i)
                        + " no se encuentra en el sistema");
                result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

                return result;
            } else {
                System.out.println("Añadiendo duración de la canción " +
                                this.songService.findById(lista.get(i)).get().getDuration()  + "segundos al total");

                totalDuration +=
                        (this.songService.findById(lista.get(i)).get().getDuration());
            }

        }


        playlistModel.setTotalDuration(totalDuration);
        this.playlistRepository.save(playlistModel);
        result.setMsg("Playlist añadida correctamente");
        result.setResponseHttpStatusCode(HttpStatus.CREATED);

        return result;
    }

    public PlaylistServiceResponse addSongToPlaylist(String id, String songId) {
        System.out.println("addSongToPlaylist en PlaylistService");
        PlaylistServiceResponse result = new PlaylistServiceResponse();
        int newDuration = 0;


        if (!findById(id).isPresent()) {
            System.out.println("No existe la playlist");
            result.setMsg("No existe la playlist");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

            return result;

        }

        PlaylistModel playlistToUpdate = findById(id).get();

        ArrayList<String> lista = playlistToUpdate.getSongList();
        if (this.songService.findById(songId).isPresent()) {

            for (int i = 0; i < lista.size(); i++) {
                if (lista.get(i).equals(songId)) {

                    System.out.println("La canción con el id " + lista.get(i)
                            + "ya se encuentra en la playlist");

                    result.setMsg("La canción con la id " + lista.get(i)
                            + " ya se encuentra en la playlist");
                    result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);
                    result.setPlaylist(playlistToUpdate);

                    return result;
                }
            }

            lista.add(songId);

            newDuration = playlistToUpdate.getTotalDuration() + this.songService.findById(songId).get().getDuration();
            playlistToUpdate.setTotalDuration(newDuration);
            playlistToUpdate.setSongList(lista);
            this.playlistRepository.save(playlistToUpdate);
            System.out.println("Canción añadida a la lista");
            result.setMsg("Canción añadida a la lista");
            result.setResponseHttpStatusCode(HttpStatus.OK);
            result.setPlaylist(playlistToUpdate);
            return result;
        }
        System.out.println("No existe la canción");
        result.setMsg("La canción no existe");
        result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);
        result.setPlaylist(playlistToUpdate);
        return result;
    }
    public boolean delete(String id){
        System.out.println("delete en PlaylistService");
        boolean result=false;
        if(this.findById(id).isPresent()==true) {
            System.out.println("Playlist encontrada, borrando");
            this.playlistRepository.deleteById(id);
            result=true;
        }

        return result;
    }

    public PlaylistModel updatePlaylist(PlaylistModel playlistModel){
        System.out.println("updatePlaylist en playlistService");
        return this.playlistRepository.save(playlistModel);
    }
    
    
}
