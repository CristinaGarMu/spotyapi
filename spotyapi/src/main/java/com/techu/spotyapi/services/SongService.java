package com.techu.spotyapi.services;

import com.techu.spotyapi.models.SongModel;
import com.techu.spotyapi.repositories.SongRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SongService {
    @Autowired
    SongRepository songRepository;

    public List<SongModel> findAll(String type) {
        List<SongModel> result;
        System.out.println("findAll en SongService");
        System.out.println("en SongService el tipo vale "+type);

        if (type == null) {
            result = this.songRepository.findAll();
        }
        else {
            result = findAllByType(type);
            return result;
        }
        return result;
    }

    public Optional<SongModel> findById(String Id) {
        System.out.println("findById en SongService");
        return this.songRepository.findById(Id);
    }

    public SongModel addSong(SongModel songModel) {
        System.out.println("addSong en SongService");
        return this.songRepository.save(songModel);
    }

    public boolean delete(String Id) {
        System.out.println("delete en  SongService");
        boolean result = false;
        if (this.findById(Id).isPresent() == true) {
            System.out.println("Canción encontrada, borrando");
            this.songRepository.deleteById(Id);
            result = true;
        }

        return result;
    }

    public SongModel updateSong(SongModel songModel) {
        System.out.println("updateSong en  SongService");
        return this.songRepository.save(songModel);
    }

    private List<SongModel> findAllByType(String type) {
        System.out.println("findAllBytype en SongService");
        System.out.println("en SongService el tipo vale "+type);
        List<SongModel> allSongs = this.songRepository.findAll();
        List<SongModel> songsWithType = new ArrayList<SongModel>() ;
        for (SongModel i : allSongs) {
            System.out.println("recorriendo lista");
            System.out.println("el tipo de la cancion es"+i.getType()+ "el solicitado es "+type);
            if (i.getType().equals(type) ) {
                songsWithType.add(i);
            }
        }
        return songsWithType;

    }
}



