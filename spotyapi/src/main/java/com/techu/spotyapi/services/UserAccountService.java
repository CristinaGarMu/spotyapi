package com.techu.spotyapi.services;

import com.techu.spotyapi.models.UserAccountModel;
import com.techu.spotyapi.repositories.UserAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserAccountService {
    @Autowired
    UserAccountRepository userAccountRepository;
    public List<UserAccountModel> findAll() {
        System.out.println("findAll en  User account service");
        return this.userAccountRepository.findAll();
    }
    public Optional<UserAccountModel> findById(String Id) {
        System.out.println("findById en  User account service");
        return this.userAccountRepository.findById(Id);
    }
    public UserAccountModel addUserAccount(UserAccountModel useraccountmodel){
        System.out.println("addUser en  User account service");
        return this.userAccountRepository.save(useraccountmodel);
    }
    public boolean delete(String Id){
        System.out.println("delete en  UserAccountService");
        boolean result=false;
        if(this.findById(Id).isPresent()==true) {
            System.out.println("Cuenta de usuario encontrada borrando");
            this.userAccountRepository.deleteById(Id);
            result=true;
        }

        return result;
    }
    public UserAccountModel updateUserAccount(UserAccountModel useraccount){
        System.out.println("updateUserAccount en  useraccount Service");
        return this.userAccountRepository.save(useraccount);
    }
}
