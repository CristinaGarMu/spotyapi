package com.techu.spotyapi.services;

import com.techu.spotyapi.models.PlaylistModel;
import org.springframework.http.HttpStatus;

public class PlaylistServiceResponse {

    private String msg;
    private PlaylistModel playlist;
    private HttpStatus responseHttpStatusCode;

    public PlaylistServiceResponse() {
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public PlaylistModel getPlaylist() {
        return playlist;
    }

    public void setPlaylist(PlaylistModel playlist) {
        this.playlist = playlist;
    }

    public HttpStatus getResponseHttpStatusCode() {
        return responseHttpStatusCode;
    }

    public void setResponseHttpStatusCode(HttpStatus responseHttpStatusCode) {
        this.responseHttpStatusCode = responseHttpStatusCode;
    }


}
