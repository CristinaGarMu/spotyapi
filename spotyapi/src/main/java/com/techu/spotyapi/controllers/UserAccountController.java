package com.techu.spotyapi.controllers;

import com.techu.spotyapi.models.UserAccountModel;
import com.techu.spotyapi.services.UserAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/spoty")
public class UserAccountController {
    @Autowired
    UserAccountService userAccountService;
    @GetMapping("/useraccounts")
    public ResponseEntity<List<UserAccountModel>> getUserAccounts(){
        System.out.println("getUsersAccount ");
        return new ResponseEntity<>(this.userAccountService.findAll(), HttpStatus.OK);
    }
    @GetMapping("/useraccounts/{id}")
    public  ResponseEntity<Object> getUserAccountById(@PathVariable String id){
        System.out.println("getAccountUser");
        System.out.println("la id de la cuenta de usuario es " +  id);
        Optional<UserAccountModel> result= this.userAccountService.findById(id);

        return new ResponseEntity<>(result.isPresent() ? result.get() : "cuenta de usuario no encontrado",
                result.isPresent() ? HttpStatus.OK: HttpStatus.NOT_FOUND);
    }
    @PutMapping  ("/useraccounts/{id}")
    public  ResponseEntity<UserAccountModel> updateUserAccount(@RequestBody UserAccountModel useraccount, @PathVariable  String id){
        System.out.println("updateAccountUser");
        System.out.println("el id de la cuenta de usuario a actualizar en parámetro URL es " +  id);
        System.out.println("el id de la cuenta de usuario a actualizar es " +  useraccount.getId());
        System.out.println("el nombre de la cuenta de usuario a actualizar es " +  useraccount.getUsername());
        System.out.println("el login de la cuenta de usuario a actualizar es " +  useraccount.getLogin());
        System.out.println("la edad de la cuenta de usuario a actualizar es " +  useraccount.getAge());
        Optional<UserAccountModel>  userAccountToUpdate= this.userAccountService.findById(id);
        if (userAccountToUpdate.isPresent()){
            System.out.println("Cuenta de usuario encontrada");
            this.userAccountService.updateUserAccount(useraccount);
        }
        return  new ResponseEntity<>(useraccount,userAccountToUpdate.isPresent() ?  HttpStatus.OK : HttpStatus.NOT_FOUND);
    }
    @DeleteMapping  ("/useraccounts/{id}")
    public  ResponseEntity<String> deleteUserAccount(@PathVariable String id){
        System.out.println("deleteUSerAccount");
        System.out.println("la id de la cuenta de usario a borrar es " +  id);
        boolean deletedUser=this.userAccountService.delete(id);

        return new ResponseEntity<>(deletedUser  ? "cuenta de usuario borrada": "No existe el usuario",
                deletedUser  ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }
    @PostMapping("/useraccounts")
    public  ResponseEntity<UserAccountModel> addUserAccount(@RequestBody UserAccountModel useraccount){
        System.out.println("addAccountuser");
        System.out.println("la id de la cuenta de usuario a añadir es " +  useraccount.getId());
        System.out.println("el name del usuario es " +  useraccount.getUsername());
        System.out.println("la edad del usuario es " +  useraccount.getAge());
        System.out.println("el login de la cuenta de usuario es " +  useraccount.getLogin());
        return  new ResponseEntity<>(this.userAccountService.addUserAccount(useraccount), HttpStatus.CREATED);
    }
}
