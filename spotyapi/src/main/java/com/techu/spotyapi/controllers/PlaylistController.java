package com.techu.spotyapi.controllers;

import com.techu.spotyapi.models.PlaylistModel;
import com.techu.spotyapi.services.PlaylistService;
import com.techu.spotyapi.services.PlaylistServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/spoty")
public class PlaylistController {

    @Autowired
    com.techu.spotyapi.services.PlaylistService playlistService;

    @GetMapping("/playlists")
    public ResponseEntity<List<PlaylistModel>> getPlaylists(@RequestParam(name="$userAccount", required = false) String userAccount){
        System.out.println("getPlaylists ");
        List<PlaylistModel> result= this.playlistService.findAll(userAccount);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/playlists/{id}")
    public  ResponseEntity<Object> getPlaylistById(@PathVariable String id){
        System.out.println("getPlaylistById");
        System.out.println("La id de la playlist es" +  id);
        Optional<PlaylistModel> result= this.playlistService.findById(id);

        return new ResponseEntity<>(result.isPresent() ? result.get() : "playlist no encontrada",
                result.isPresent() ? HttpStatus.OK: HttpStatus.NOT_FOUND);
    }
    @PutMapping("/playlists/{id}")
    public  ResponseEntity<PlaylistModel> updatePlaylist(@RequestBody PlaylistModel playlist, @PathVariable  String id){
        System.out.println("updatePlaylist");
        System.out.println("El id de la playlist a actualizar en parámetro URL es " +  id);
        System.out.println("El id de la playlist a actualizar es " +  playlist.getId());
        System.out.println("El userId de la playlist a actualizar es " +  playlist.getUserId());
        System.out.println("El name de la playlist a actualizar es " +  playlist.getName());
        System.out.println("La duración de la playlist a actualizar es " +  playlist.getTotalDuration());
        System.out.println("La lista de canciones de la playlist a actualizar es " +  playlist.getSongList());

        Optional<PlaylistModel>  PlaylistToUpdate= this.playlistService.findById(id);
        if (PlaylistToUpdate.isPresent()){
            System.out.println("Playlist encontrada");
            this.playlistService.updatePlaylist(playlist);
        }
        return  new ResponseEntity<>(playlist,PlaylistToUpdate.isPresent() ?  HttpStatus.OK : HttpStatus.NOT_FOUND);
    }


    @PutMapping("/playlists")
    public  ResponseEntity<Object> addSongToPlaylist(@RequestParam(name="$songId", required = true) String songId, @RequestParam(name="$playlistId", required = true) String id) {
        System.out.println("addSongToPlaylist");
        System.out.println("El id de la playlist a actualizar en parámetro URL es " + id);
        System.out.println("El id de la canción a añadir a la playlist es " + songId);


        PlaylistServiceResponse result = this.playlistService.addSongToPlaylist(id, songId);

        return new ResponseEntity<>(result, result.getResponseHttpStatusCode());
    }

    @DeleteMapping  ("/playlists/{id}")
    public  ResponseEntity<String> deletePlaylist(@PathVariable String id){
        System.out.println("deletePlaylist");
        System.out.println("la id de la playlist a borrar es " +  id);
        boolean deletedPlaylist=this.playlistService.delete(id);

        return new ResponseEntity<>(deletedPlaylist  ? "Playlist borrada": "No existe la playlist",
                deletedPlaylist  ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @PostMapping("/playlists")
    public  ResponseEntity<PlaylistServiceResponse> addPlaylist(@RequestBody PlaylistModel playlist){
        System.out.println("addPlaylist");
        System.out.println("El id de la playlist a crear es " +  playlist.getId());
        System.out.println("El userId de la playlist a crear es " +  playlist.getUserId());
        System.out.println("El name de la playlist a crear es " +  playlist.getName());
        System.out.println("La duración de la playlist a crear es " +  playlist.getTotalDuration());
        System.out.println("La lista de canciones de la playlist a crear es " +  playlist.getSongList());

        PlaylistServiceResponse resultAdd = this.playlistService.addPlaylist(playlist);
        return  new ResponseEntity<>(resultAdd, resultAdd.getResponseHttpStatusCode());
    }
    
    
}
