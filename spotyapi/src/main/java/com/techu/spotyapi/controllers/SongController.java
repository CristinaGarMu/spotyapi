package com.techu.spotyapi.controllers;

import com.techu.spotyapi.models.SongModel;
import com.techu.spotyapi.services.SongService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/spoty")
public class SongController {

    @Autowired
    SongService SongService;

    @GetMapping("/songs")
    public ResponseEntity<List<SongModel>> getSongs(@RequestParam(name="$songType", required = false) String type){
        System.out.println("getSongs ");
        System.out.println("el tipo vale "+type);
        List<SongModel> result= this.SongService.findAll(type);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
    /*@GetMapping("/songs")
    public ResponseEntity<List<SongModel>> getSongsByType(@RequestParam(name="$songType", required = true) String type) {
        System.out.println("getSongsByType");
        List<SongModel> result= this.SongService.findAllByType(type);
        return new ResponseEntity<>(result,HttpStatus.OK);
    }*/

    @GetMapping("/songs/{id}")
    public  ResponseEntity<Object> getSongById(@PathVariable String id){
        System.out.println("getSongById");
        System.out.println("La id de la canción es" +  id);
        Optional<SongModel> result= this.SongService.findById(id);

        return new ResponseEntity<>(result.isPresent() ? result.get() : "canción no encontrada",
                result.isPresent() ? HttpStatus.OK: HttpStatus.NOT_FOUND);
    }
    @PutMapping("/songs/{id}")
    public  ResponseEntity<SongModel> updateSong(@RequestBody SongModel song, @PathVariable  String id){
        System.out.println("updateSong");
        System.out.println("El id de la canción a actualizar en parámetro URL es " +  id);
        System.out.println("El id de la canción a actualizar es " +  song.getId());
        System.out.println("El nombre de la canción a actualizar es " +  song.getName());
        System.out.println("El autor de la canción a actualizar es " +  song.getAuthor());
        System.out.println("El album de la canción a actualizar es " +  song.getAlbum());
        System.out.println("La duración de la canción a actualizar es " +  song.getDuration());
        System.out.println("El genero de la canción a actualizar es " +  song.getType());

        Optional<SongModel>  SongToUpdate= this.SongService.findById(id);
        if (SongToUpdate.isPresent()){
            System.out.println("Canción encontrada");
            this.SongService.updateSong(song);
        }
        return  new ResponseEntity<>(song,SongToUpdate.isPresent() ?  HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @DeleteMapping  ("/songs/{id}")
    public  ResponseEntity<String> deleteSong(@PathVariable String id){
        System.out.println("deleteSong");
        System.out.println("la id de la canción a borrar es " +  id);
        boolean deletedSong=this.SongService.delete(id);

        return new ResponseEntity<>(deletedSong  ? "Canción borrada": "No existe la canción",
                deletedSong  ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @PostMapping("/songs")
    public  ResponseEntity<SongModel> addSong(@RequestBody SongModel song){
        System.out.println("addSong");
        System.out.println("El id de la canción a crear es " +  song.getId());
        System.out.println("El nombre de la canción a crear es " +  song.getName());
        System.out.println("El autor de la canción a crear es " +  song.getAuthor());
        System.out.println("El album de la canción a crear es " +  song.getAlbum());
        System.out.println("La duración de la canción a crear es " +  song.getDuration());
        System.out.println("El genero de la canción a crear es " +  song.getType());
        return  new ResponseEntity<>(this.SongService.addSong(song), HttpStatus.CREATED);
    }
}
