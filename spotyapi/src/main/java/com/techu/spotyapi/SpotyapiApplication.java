package com.techu.spotyapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpotyapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpotyapiApplication.class, args);
	}

}
