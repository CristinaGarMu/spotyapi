package com.techu.spotyapi.repositories;

import com.techu.spotyapi.models.PlaylistModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlaylistRepository extends MongoRepository<PlaylistModel, String> {
}
