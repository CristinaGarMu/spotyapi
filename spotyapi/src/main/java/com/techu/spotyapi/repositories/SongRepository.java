package com.techu.spotyapi.repositories;

import com.techu.spotyapi.models.SongModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface SongRepository extends MongoRepository<SongModel, String> {
}
