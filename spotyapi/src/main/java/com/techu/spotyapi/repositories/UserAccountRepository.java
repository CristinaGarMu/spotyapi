package com.techu.spotyapi.repositories;

import com.techu.spotyapi.models.UserAccountModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface UserAccountRepository extends MongoRepository<UserAccountModel, String> {
}
