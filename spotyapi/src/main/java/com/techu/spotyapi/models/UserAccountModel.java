package com.techu.spotyapi.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "usersaccounts")
public class UserAccountModel {
    @Id
    private String id;
    private String login;
    private String username;
    private int age;

    public UserAccountModel() {
    }

    public UserAccountModel(String id, String login, String username, int age) {
        this.id = id;
        this.login = login;
        this.username = username;
        this.age = age;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
