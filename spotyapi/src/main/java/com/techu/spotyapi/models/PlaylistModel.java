package com.techu.spotyapi.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;

@Document(collection = "playlists")
public class PlaylistModel {

    @Id

    private String id;
    private String userId;
    private String name;
    private int totalDuration;
    private ArrayList<String> songList;

    public PlaylistModel() {
    }

    public PlaylistModel(String id, String userId, String name, int totalDuration, ArrayList songList) {
        this.id = id;
        this.userId = userId;
        this.name = name;
        this.totalDuration = totalDuration;
        this.songList = songList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotalDuration() {
        return totalDuration;
    }

    public void setTotalDuration(int totalDuration) {
        this.totalDuration = totalDuration;
    }

    public ArrayList getSongList() {
        return songList;
    }

    public void setSongList(ArrayList songList) {
        this.songList = songList;
    }
}
